package com.zetahealth.pharmaapp.data.api;

import com.zetahealth.pharmaapp.data.api.responses.DefaultResponse;

public class ApiClientListener {
    public interface AccountRegistrationListener {
        void onAccountRegistered(DefaultResponse defaultResponse);
    }
}
