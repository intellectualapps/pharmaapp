package com.zetahealth.pharmaapp.data.api;

import android.os.AsyncTask;

import com.zetahealth.pharmaapp.PharmaAppApplication;
import com.zetahealth.pharmaapp.data.api.responses.DefaultResponse;
import com.zetahealth.pharmaapp.utils.ApiUtils;
import com.zetahealth.pharmaapp.utils.Constants;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Converter;
import retrofit2.Response;

public class ApiClient {
    public static class NetworkCallsRunner extends AsyncTask<Void, Void, Void> {
        private String REQUEST_TYPE;
        private Map<String, String> requestMap;
        private String emailAddress, username, invitorUsername, inviteeEmail, categoryIds, pageNumber, pageSize, query, charityId, id, requestingParticipant, otherParticipant;
        private String follower, followed;
        private boolean state;
        private Map<String, RequestBody> partDataMap;


        private ApiClientListener.AccountRegistrationListener mAccountRegistrationListener;

        private DefaultResponse defaultResponse;

        private static ApiService apiService = null;


        public static void resetApiService() {
            apiService = null;
        }


        public NetworkCallsRunner(String REQUEST_TYPE, Map<String, String> accountMap, ApiClientListener.AccountRegistrationListener accountRegistrationListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.requestMap = accountMap;
            this.mAccountRegistrationListener = accountRegistrationListener;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            apiService = getApiService();
        }

        private static ApiService getApiService() {
            if (apiService == null) {
                apiService = ApiModule.getApiModuleInstance(PharmaAppApplication.getCacheFile(), ApiUtils.buildHeaders(null)).getApiService();
            }

            return apiService;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            switch (REQUEST_TYPE) {
                case Constants.REGISTER_REQUEST:
                    defaultResponse = registerUserAccount(requestMap);
                    break;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            switch (REQUEST_TYPE) {
                case Constants.REGISTER_REQUEST:
                    mAccountRegistrationListener.onAccountRegistered(defaultResponse);
                    break;
            }
        }

        private static DefaultResponse registerUserAccount(Map<String, String> accountMap) {
            Call<DefaultResponse> call;
            DefaultResponse defaultResponse = null;

            try {
                call = getApiService().createAccount(accountMap);
                Response<DefaultResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        defaultResponse = response.body();
                    } else {
                        Converter<ResponseBody, DefaultResponse> converter =
                                ApiModule.buildRetrofitAdapter(PharmaAppApplication.getCacheFile(), null)
                                        .responseBodyConverter(DefaultResponse.class, new Annotation[0]);
                        try {
                            defaultResponse = converter.convert(response.errorBody());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return defaultResponse;
        }
    }
}