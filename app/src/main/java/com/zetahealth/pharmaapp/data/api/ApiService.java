package com.zetahealth.pharmaapp.data.api;

import com.zetahealth.pharmaapp.data.api.responses.DefaultResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;


interface ApiService {
    @POST("/api/v1/user")
    Call<DefaultResponse> createAccount(@QueryMap Map<String, String> dataMap);
}
