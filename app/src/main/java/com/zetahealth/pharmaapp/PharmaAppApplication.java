package com.zetahealth.pharmaapp;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.zetahealth.pharmaapp.ui.activities.LaunchActivity;

import java.io.File;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


public class PharmaAppApplication extends Application {
    static File cacheFile;
    private static PharmaAppApplication pharmaAppApplication;

    @Override
    public void onCreate() {
        super.onCreate();
        cacheFile = new File(getCacheDir(), "responses");
        pharmaAppApplication = this;
        initUncaughtExceptionHandler();
    }


    public static File getCacheFile() {
        return cacheFile;
    }


    public static PharmaAppApplication getAppInstance() {
        return pharmaAppApplication;
    }

    private void initUncaughtExceptionHandler() {
        final ScheduledThreadPoolExecutor c = new ScheduledThreadPoolExecutor(1);
        c.schedule(new Runnable() {
            @Override
            public void run() {
                final Thread.UncaughtExceptionHandler defaultHandler = Thread.getDefaultUncaughtExceptionHandler();
                Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                    @Override
                    public void uncaughtException(Thread paramThread, Throwable paramThrowable) {
                        Intent intent = new Intent(getApplicationContext(), LaunchActivity.class);

                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        PendingIntent pendingIntent = PendingIntent.getActivity(getAppInstance().getApplicationContext(), 0, intent, PendingIntent.FLAG_ONE_SHOT);

                        AlarmManager mgr = (AlarmManager) getAppInstance().getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                        if (mgr != null) {
                            mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 2000, pendingIntent);
                        }

                        defaultHandler.uncaughtException(paramThread, paramThrowable);
                    }
                });
            }
        }, 5, TimeUnit.SECONDS);
    }
}
