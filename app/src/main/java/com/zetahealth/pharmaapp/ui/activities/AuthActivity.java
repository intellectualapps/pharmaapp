package com.zetahealth.pharmaapp.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.zetahealth.pharmaapp.R;
import com.zetahealth.pharmaapp.data.models.User;
import com.zetahealth.pharmaapp.ui.fragments.LoginFragment;
import com.zetahealth.pharmaapp.utils.Constants;
import com.zetahealth.pharmaapp.utils.PreferenceStorageManager;


public class AuthActivity extends BaseActivity implements View.OnClickListener {
    private Bundle fragmentBundle;
    private String viewType;
    private User user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setElevation(0);

        notificationViewPopup = findViewById(R.id.notification_view_container);
        notificationViewPopup.setOnClickListener(this);
        notificationTitleView = (TextView) notificationViewPopup.findViewById(R.id.notification_title);
        notificationMessageView = (TextView) notificationViewPopup.findViewById(R.id.notification_message);

        if (getIntent().getExtras() != null) {
            Intent intent = getIntent();
            viewType = intent.getExtras().getString(Constants.VIEW_TYPE);
            fragmentBundle = intent.getExtras();
        }

        if (PreferenceStorageManager.getLoggedInStatus(getApplicationContext())) {
            viewType = Constants.LOGIN_VIEW_TAG;
        }

        if (savedInstanceState == null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment frag = null;
            if (viewType != null) {
                switch (viewType) {
                    case Constants.LOGIN_VIEW_TAG:
                        LoginFragment.newInstance(fragmentBundle);
                        break;
                    default:
                        frag = LoginFragment.newInstance(fragmentBundle);
                        break;
                }
            } else {
                frag = LoginFragment.newInstance(null);
            }

            ft.replace(R.id.container, frag, frag.getClass().getSimpleName());
            ft.commit();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Fragment fragment = getSupportFragmentManager().findFragmentByTag(LoginFragment.class.getSimpleName());
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.notification_view_container:
                notificationViewPopup.setVisibility(View.GONE);
                break;
        }
    }
}
