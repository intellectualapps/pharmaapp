package com.zetahealth.pharmaapp.ui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;


import com.zetahealth.pharmaapp.PharmaAppApplication;
import com.zetahealth.pharmaapp.R;
import com.zetahealth.pharmaapp.data.models.User;
import com.zetahealth.pharmaapp.utils.Constants;
import com.zetahealth.pharmaapp.utils.PreferenceStorageManager;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;


public class BaseActivity extends AppCompatActivity {
    private static final String TAG = BaseActivity.class.getSimpleName();
    public View notificationViewPopup;
    public Toolbar toolbar;
    public TextView notificationTitleView, notificationMessageView;
    private List<WeakReference<Fragment>> mFragList = new ArrayList<WeakReference<Fragment>>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    public void showMainActivity(User user) {
        if (user == null) {
            return;
        }

        Intent intent = null;
        Context context = PharmaAppApplication.getAppInstance().getApplicationContext();

        if (!PreferenceStorageManager.isFirstUseOnDevice(context)) {
            intent = new Intent(context, MainActivity.class);
        } else {
            intent = new Intent(context, MainActivity.class);
        }

        intent.putExtra(Constants.USER, user);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    public void fireNotificationPopup(String title, String message) {
        notificationViewPopup.setVisibility(View.VISIBLE);
        notificationViewPopup.bringToFront();
        notificationViewPopup.invalidate();
        notificationTitleView.setText(title);
        notificationMessageView.setText(message);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                notificationViewPopup.setVisibility(View.GONE);
            }
        }, 5000);
    }


    public Toolbar getToolbar() {
        if (toolbar == null) {
            toolbar = (Toolbar) findViewById(R.id.toolbar);
        }
        return toolbar;
    }

    public void initializeToolbar(String title) {
        getToolbar().setVisibility(View.VISIBLE);
        getToolbar().setTitle(title);
        getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateUp();
            }
        });
    }

    public void navigateUp() {
        int backstack = getSupportFragmentManager().getBackStackEntryCount();
        if (backstack > 0) {
            //just pop
            getSupportFragmentManager().popBackStack();
        } else {
            finish();
        }
    }

    public void unregisterReceiver(Context context, BroadcastReceiver broadcastReceiver) {
        try {
            context.unregisterReceiver(broadcastReceiver);
        } catch (Exception ignored) {
        }
    }

    public Fragment findActiveFragmentByTag(String fragmentTag, FragmentManager fragmentManager) {
        List<Fragment> activeFragments = getActiveFragments();
        if (fragmentManager != null && activeFragments != null && activeFragments.size() > 0) {
            for (Fragment fragment : activeFragments) {
                if (fragment != null && fragment.getTag().contains(fragmentTag)) {
                    return fragment;
                }
            }
        }
        return fragmentManager != null ? fragmentManager.findFragmentByTag(fragmentTag) : null;
    }

    @Override
    public void onAttachFragment(android.app.Fragment fragment) {
        mFragList.add(new WeakReference(fragment));
    }

    public List<Fragment> getActiveFragments() {
        ArrayList<Fragment> ret = new ArrayList<Fragment>();
        for (WeakReference<Fragment> ref : mFragList) {
            Fragment f = ref.get();
            if (f != null) {
                if (f.isVisible()) {
                    ret.add(f);
                }
            }
        }
        return ret;
    }
}
