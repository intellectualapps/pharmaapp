package com.zetahealth.pharmaapp.ui.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import com.zetahealth.pharmaapp.PharmaAppApplication;
import com.zetahealth.pharmaapp.R;
import com.zetahealth.pharmaapp.data.models.User;
import com.zetahealth.pharmaapp.ui.activities.BaseActivity;
import com.zetahealth.pharmaapp.ui.activities.MainActivity;
import com.zetahealth.pharmaapp.utils.AppUtils;
import com.zetahealth.pharmaapp.utils.Constants;
import com.zetahealth.pharmaapp.utils.Validator;

public class BaseFragment extends Fragment {
    private ProgressDialog progressDialog;
    public View mSnackBarView;
    public Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
    }


    public void closeFragment() {
        if (getActivity() != null) {
            if (getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0) {
                hideKeyboard();
                getActivity().getSupportFragmentManager().popBackStack();
            } else {
                hideKeyboard();
                getActivity().finish();
            }
        }
    }

    public void showLoadingIndicator(boolean showProgressLoader, String message) {

        try {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
            }

            progressDialog.setMessage(message);
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);

            if (showProgressLoader) {
                progressDialog.show();
            } else {
                if (progressDialog.isShowing()) {
                    progressDialog.cancel();
                }
            }
        } catch (Exception e) {

        }
    }

    public void startFragment(Fragment frag) {
        if (getActivity() != null && frag != null) {
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            fragmentTransaction.replace(R.id.container, frag, frag.getClass().getName());
            fragmentTransaction.addToBackStack(frag.getClass().getName());
            fragmentTransaction.commit();
        }
    }

    public void switchFragment(Fragment frag) {
        if (getActivity() != null && frag != null) {
            FragmentTransaction fragmentTransaction = ((BaseActivity) getActivity()).getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            fragmentTransaction.replace(R.id.container, frag, frag.getClass().getName());
            fragmentTransaction.addToBackStack(frag.getClass().getName());
            fragmentTransaction.commit();
        }
    }

    private void clearBackStack(FragmentManager fragmentManager) {
        if (fragmentManager.getBackStackEntryCount() > 0) {
            for (int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
                while (fragmentManager.getBackStackEntryCount() > 0) {
                    fragmentManager.popBackStackImmediate();
                }
            }
        }
        fragmentManager.getBackStackEntryCount();
    }


    public Context getMinBarApplicationContext() {
        return PharmaAppApplication.getAppInstance().getApplicationContext();
    }


    protected void hideKeyboard() {
        if (getActivity() != null) {
            AppUtils.hideKeyboard(getActivity());
        }
    }


    public void showMainActivity(User user, Activity activity) {
        if (user == null) {
            return;
        }
        if (activity == null || activity.isFinishing()) {
            return;
        }
        hideKeyboard();
        Intent intent = null;
        intent = new Intent(activity, MainActivity.class);

        intent.putExtra(Constants.USER, user);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        activity.finish();
    }




    public boolean validateFields(EditText[] views) {
        return Validator.validateInputViewsNotEmpty(views);
    }

    public boolean validateEmailInput(EditText view) {
        return Validator.validateEmailAddress(view);
    }

    public boolean validateTextInput(EditText view) {
        return Validator.validateInputNotEmpty(view);
    }

    public boolean validatePasswordViews(EditText passwordInput, EditText confirmPasswordInput) {
        return Validator.validatePasswordsEqual(passwordInput, confirmPasswordInput);
    }

    public boolean validateInputLength(EditText editText, int length) {
        return Validator.validateInputLength(editText, length);
    }

    public boolean validatePhoneNumber(EditText view) {
        return Validator.validatePhoneNumber(view);
    }

    public boolean validateAmountView(EditText view) {
        return false;
    }


}
