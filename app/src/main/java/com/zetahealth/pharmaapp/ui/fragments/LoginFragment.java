package com.zetahealth.pharmaapp.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.zetahealth.pharmaapp.R;
import com.zetahealth.pharmaapp.data.models.User;
import com.zetahealth.pharmaapp.ui.activities.BaseActivity;
import com.zetahealth.pharmaapp.utils.CommonUtils;
import com.zetahealth.pharmaapp.utils.Constants;
import com.zetahealth.pharmaapp.utils.NetworkUtils;
import com.zetahealth.pharmaapp.utils.PreferenceStorageManager;

import java.util.HashMap;
import java.util.Map;

public class LoginFragment extends BaseFragment implements View.OnClickListener {
    private User user;
    private TextView resetPasswordButton;
    private EditText emailAddressInput, passwordInput;
    private Button loginButton;
    private Toolbar toolbar;


    public static Fragment newInstance(Bundle args) {
        Fragment frag = new LoginFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public LoginFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mSnackBarView = view;
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).getToolbar().setVisibility(View.GONE);
        resetPasswordButton = (TextView) view.findViewById(R.id.forgot_password_button);
        emailAddressInput = (EditText) view.findViewById(R.id.email_address);
        passwordInput = (EditText) view.findViewById(R.id.password);
        loginButton = (Button) view.findViewById(R.id.login_button);

        resetPasswordButton.setOnClickListener(this);
        loginButton.setOnClickListener(this);
        loadSavedLoginData();
    }

    @Override
    public void onClick(View v) {
        String message = getString(R.string.login_progress_label);
        Bundle bundle = new Bundle();

        switch (v.getId()) {
            case R.id.login_button:
                if (validateEmailInput(emailAddressInput) && validateTextInput(passwordInput)) {
                    if (NetworkUtils.isConnected(getContext())) {
                        hideKeyboard();
                        showLoadingIndicator(true, message);
                        // PreferenceStorageManager.setLoginData(getContext().getApplicationContext(), getUserInput().get(Constants.ID), getUserInput().get(Constants.PASSWORD));
                        makeAPICall(getUserInput());
                    } else {
                        CommonUtils.displaySnackBarMessage(mSnackBarView, getString(R.string.network_connection_label));
                    }
                }
                break;
        }
    }

    public Map<String, String> getUserInput() {
        Map<String, String> map = new HashMap<String, String>();
        String password = passwordInput.getText().toString().trim();
        String emailAddress = emailAddressInput.getText().toString().trim();
        map.put(Constants.PASSWORD, password);
        map.put(Constants.ID, emailAddress);
        map.put(Constants.AUTH_TYPE, Constants.EMAIL_AUTH_TYPE);
        return map;
    }

    private void loadSavedLoginData() {
        Map<String, String> loginData = new HashMap<String, String>();
        loginData = PreferenceStorageManager.getLoginData(getContext().getApplicationContext());
        if (loginData != null && loginData.size() > 0) {
            emailAddressInput.setText(loginData.get(Constants.ID));
            passwordInput.setText(loginData.get(Constants.PASSWORD));
        }
    }

    private void makeAPICall(Map<String, String> accountMap) {
        showMainActivity(new User(), getActivity());
        /*new ApiClient.NetworkCallsRunner(Constants.LOGIN_REQUEST, accountMap, new ApiClientListener.AccountAuthenticationListener() {
            @Override
            public void onAccountAuthenticated(LoginResponse loginResponse) {
                showLoadingIndicator(false, "");
                if (loginResponse != null) {
                    PreferenceStorageManager.setHasLoggedInOnce(ZakatifyApplication.getAppInstance().getApplicationContext(), true);
                    if (loginResponse.getStatus() == null && loginResponse.getMessage() == null) {
                        User user = extractUserDetails(loginResponse);

                        ApiClient.NetworkCallsRunner.resetApiService();
                        ApiModule.resetApiClient();
                        saveUserData(ZakatifyApplication.getAppInstance().getApplicationContext(), user);
                        showMainActivity(user, getActivity());
                    } else {
                        if (isAdded() && getActivity() != null) {
                            showPopupMessage(loginResponse.getMessage());
                        }
                    }
                } else {
                    showPopupMessage(getString(R.string.null_response_label));
                }
            }
        }).execute();*/
    }
}
