package com.zetahealth.pharmaapp.utils;

import android.content.Context;

import com.zetahealth.pharmaapp.PharmaAppApplication;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.Buffer;

public class ApiUtils {
    private static final String HEADER_PLATFORM = "X-Platform";
    private static final String HEADER_DEVICE_ID = "X-Native-Device-Id";
    private static final String HEADER_USER_LATITUDE = "X-Coord-Lat";
    private static final String HEADER_USER_LONGITUDE = "X-Coord-Lon";
    private static final String HEADER_AUTHORIZATION = "Authorization";
    private static final String HEADER_APP_VERSION = "X-App-Version";

    private static final String APP_PLATFORM = "1";

    public static Map<String, String> buildHeaders(Map<String, String> extraHeaders) {
        Map<String, String> headers = new HashMap<String, String>();

        Context context = PharmaAppApplication.getAppInstance().getApplicationContext();

        headers.put(HEADER_DEVICE_ID, StringUtils.nullify(PreferenceStorageManager.getDeviceId(context)));
        headers.put(HEADER_PLATFORM, APP_PLATFORM);
        headers.put(HEADER_APP_VERSION, StringUtils.nullify(AppUtils.getAppVersion(context)));
        headers.put(HEADER_AUTHORIZATION, "Bearer: " + StringUtils.nullify(PreferenceStorageManager.getAuthToken(context)));

        if (extraHeaders != null)
            headers.putAll(extraHeaders);

        return headers;
    }

    public static RequestBody getRequestBody(String value) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), value);
        return requestBody;
    }


    public static String bodyToString(final RequestBody request) {
        try {
            final RequestBody copy = request;
            final Buffer buffer = new Buffer();
            copy.writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "";
        } catch (Exception e) {
            return "";
        }
    }
}
