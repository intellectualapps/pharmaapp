package com.zetahealth.pharmaapp.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Network related utils
 */
public class NetworkUtils {


    /**
     * Checks whether network is connected
     *
     * @param context context
     * @return true if the network is connected, false otherwise
     */
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public static boolean isConnected(Context context) {
        if (context != null) {
            ConnectivityManager connectivity = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo networkInfo = connectivity.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()
                    && networkInfo.isAvailable() && networkInfo.isConnectedOrConnecting()) {
                return true;
            }
            return false;
        } else {
            return false;
        }
    }

    public static boolean hasInternetAccess(Context context) {
        if (isConnected(context)) {
            try {
                HttpURLConnection urlConnection = (HttpURLConnection) (new URL("http://clients3.google.com/generate_204")).openConnection();
                urlConnection.setRequestProperty("User-Agent", "Android");
                urlConnection.setRequestProperty("Connection", "close");
                urlConnection.setConnectTimeout(1500);
                urlConnection.connect();
                return (urlConnection.getResponseCode() == 204 && urlConnection.getContentLength() == 0);
            } catch (Exception e) {
                Log.e(NetworkUtils.class.getSimpleName(), "Error checking internet connection", e);
            }
        } else {
            Log.d(NetworkUtils.class.getSimpleName(), "No Network available");
        }
        return false;
    }
}
