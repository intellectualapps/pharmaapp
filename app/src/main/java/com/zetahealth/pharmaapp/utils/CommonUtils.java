package com.zetahealth.pharmaapp.utils;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Toast;

import com.zetahealth.pharmaapp.PharmaAppApplication;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

public class CommonUtils {
    public static void displaySnackBarMessage(View mSnackBarAnchor, String message) {
        if (mSnackBarAnchor != null && mSnackBarAnchor.getContext() != null) {
            Snackbar.make(mSnackBarAnchor, message, Snackbar.LENGTH_LONG).show();
        }
    }

    public static void displayShortToastMessage(String message) {
        Context context = PharmaAppApplication.getAppInstance().getApplicationContext();
        if (context != null) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }

    public static void displayLongToastMessage(String message) {
        Context context = PharmaAppApplication.getAppInstance().getApplicationContext();
        if (context != null) {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }

    public static String formatAmountToCurrency(Double amount) {
        if (amount == null) {
            return "0.00";
        }
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.getDefault());
        DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) numberFormat).getDecimalFormatSymbols();
        decimalFormatSymbols.setCurrencySymbol("");
        ((DecimalFormat) numberFormat).setDecimalFormatSymbols(decimalFormatSymbols);
        numberFormat.setMinimumFractionDigits(2);
        return numberFormat.format(amount);
    }

    public static String formatAmountToCurrencyWithDecimalPlaces(Object amount) {
        if (amount == null) {
            return "0.00";
        }
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.getDefault());
        DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) numberFormat).getDecimalFormatSymbols();
        decimalFormatSymbols.setCurrencySymbol("");
        ((DecimalFormat) numberFormat).setDecimalFormatSymbols(decimalFormatSymbols);
        numberFormat.setMinimumFractionDigits(2);
        return numberFormat.format(amount);
    }
}
