package com.zetahealth.pharmaapp.utils;

public class Constants {
    public static final String USER = "user";
    public static final String EMAIL_ADDRESS = "email";
    public static final String PLATFORM = "platform";
    public static final String FIRST_NAME = "first-name";
    public static final String LAST_NAME = "last-name";
    public static final String FULL_NAME = "full-name";
    public static final String FACEBOOK_ACCESS_TOKEN = "fb_token";
    public static final String FACEBOOK_ID = "fb_id";
    public static final String PHONE_NUMBER = "phone-number";
    public static final String USER_TYPE = "user-type";
    public static final String USER_EMAIL = "user-email";
    public static final String PASSWORD = "password";
    public static final String USERNAME = "username";
    public static final String MAIN_USERNAME = "main_username";
    public static final String ID = "id";
    public static final String AUTH_TYPE = "auth-type";
    public static final String EMAIL_AUTH_TYPE = "email";
    public static final String FACEBOOK_AUTH_TYPE = "facebook";
    public static final String TWITTER_AUTH_TYPE = "twitter";
    public static final String GENDER = "gender";
    public static final String PROFILE_PHOTO = "profile-photo";
    public static final String LOCATION = "location";
    public static final String CATEGORY_ID = "category-id";
    public static final String CATEGORY_IDS = "category-ids";
    public static final String PHOTO_URL = "photo-url";
    public static final String TEMP_KEY = "temp-key";
    public static final String AMOUNT = "amount";
    public static final String FREQUENCY_ID = "frequency-id";
    public static final String PERIOD = "period";


    public static final String PASSWORD_VALIDATION_REGEX = "^[ A-Za-z0-9_@.\\/#&+-]{6,}$";

    /*
    Push Notification Keys
     */

    public static final String PUSH_OFFLINE_EMAIL = "offlineUserEmail";
    public static final String PUSH_ONLINE_EMAIL = "onlineUserEmail";
    public static final String PUSH_PHOTO_URL = "photoUrl";
    public static final String PUSH_SENDER_FULL_NAME = "senderFullName";
    public static final String PUSH_NOTIFICATION_PLATFORM = "notificationPlatform";
    public static final String PUSH_MESSAGE_ID = "messageId";
    public static final String CONNECTION_ID = "connection-id";


    /*
    Chat Related
     */
    public static final String CHAT_SESSIONS = "connections";
    public static final String CHAT_SESSION = "chat_session";
    public static final String CHAT_SESSION_MESSAGES_KEY = "messages";
    public static final String CHAT_SESSION_PARTICIPANTS_KEY = "participants";
    public static final String MESSAGE_ID = "id";
    public static final String REQUESTING_PARTICIPANT = "requesting-participant";
    public static final String OTHER_PARTICIPANT = "other-participant";
    public static final String MESSAGE_NOTIFICATION = "offline-user";
    public static final String HTML_LINE_BREAK = "<br/>";

    public static final String MESSAGE_ID_KEY = "id";
    public static final String MESSAGE_AUTHOR_KEY = "author";
    public static final String MESSAGE_TEXT_KEY = "text";
    public static final String MESSAGE_TIME_KEY = "time";


    public static final String PROFILE_DATA = "profile-data";
    public static final String ONBOARDING_CONTENT = "onboarding-content";
    public static final String REFRESH_FLAG = "refresh";
    public static final String STATUS = "status";
    public static final String DOWNLOAD_COMPLETE = "complete";
    public static final String DOWNLOAD_FAILED = "failed";
    public static final String DOWNLOAD_URI = "download-uri";
    public static final String FILE_PATH = "file-path";


    public static final String VIEW_TYPE = "viewType";
    public static final String REGISTER_REQUEST = "REGISTER";
    public static final String LOGIN_REQUEST = "LOGIN";
    public static final String UPLOAD_FCM_TOKEN_REQUEST = "UPLOAD_FCM_TOKEN";


    /*
    Cloud Messaging
     */
    public static final String NOTIFICATION_TYPE = "notification-type";
    public static final String NOTIFICATION_MESSAGE = "notification-message";
    public static final String NOTIFICATION_USER_PARAM = "user-param";
    public static final String MESSAGE = "message";
    public static final String TITLE = "title";
    public static final String NOTIFICATION_FLAG = "NOTIFICATION_FLAG";


    public static final String DEVICE_PLATFORM = "1";
    public static final String DEVICE_PLATFORM_KEY = "platform";
    public static final String FCM_TOKEN = "token";
    public static final String DEVICE_ID = "uuid";

    /*
    View Tags
     */
    public static final String WELCOME_VIEW_TAG = "WELCOME_VIEW";
    public static final String LOGIN_VIEW_TAG = "LOGIN_VIEW";
    public static final String SEARCH_VIEW_TAG = "SEARCH_VIEW";
    public static final String ABOUT_VIEW_TAG = "ABOUT_VIEW";
}