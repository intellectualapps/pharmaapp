package com.zetahealth.pharmaapp.utils;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;
import java.util.Map;

public class FontUtils {
    private static String FONT_REGULAR = "FiraSansCondensed-Regular.ttf";
    private static String FONT_MEDIUM = "FiraSansCondensed-Medium.ttf";
    private static String FONT_BOLD = "FiraSansCondensed-Bold.ttf";

    public static int STYLE_REGULAR = 0;
    public static int STYLE_MEDIUM = 1;
    public static int STYLE_BOLD = 2;

    private static Map<String, Typeface> sCachedFonts = new HashMap<String, Typeface>();

    public static Typeface getTypeface(Context context, String assetPath) {
        if (!sCachedFonts.containsKey(assetPath)) {
            Typeface tf = Typeface.createFromAsset(context.getAssets(), assetPath);
            sCachedFonts.put(assetPath, tf);
        }
        return sCachedFonts.get(assetPath);
    }

    public static Typeface selectTypeface(Context context, int textStyle) {
        String firaSansDirectory = "fonts/";
        String font;
        switch (textStyle) {
            default:
            case 0:
                font = FontUtils.FONT_REGULAR;
                break;
            case 1:
                font = FontUtils.FONT_MEDIUM;
                break;
            case 2:
                font = FontUtils.FONT_BOLD;
                break;
        }

        return FontUtils.getTypeface(context, firaSansDirectory + font);
    }
}
