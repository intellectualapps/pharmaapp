package com.zetahealth.pharmaapp.utils;

import android.content.Context;


import com.zetahealth.pharmaapp.data.models.User;

import java.util.HashMap;
import java.util.Map;

public class PreferenceStorageManager {
    private static final String PREFS_FCM_TOKEN = "prefs_fcm_token";
    private static final String PREFS_AUTH_TOKEN = "prefs_auth_token";

    private static final String PREFS_DEVICE_ID = "prefs_device_id";
    private static final String PREFS_FIRST_NAME = "prefs_first_name";
    private static final String PREFS_GENDER = "prefs_gender";
    private static final String PREFS_LAST_NAME = "prefs_last_name";
    private static final String PREFS_PROFILE_PHOTO = "prefs_profile_photo";
    private static final String PREFS_USERNAME = "prefs_username";
    private static final String PREFS_ID = "prefs_id";
    private static final String PREFS_EMAIL_ADDRESS = "prefs_email_address";
    private static final String PREFS_TWITTER_EMAIL_ADDRESS = "prefs_twitter_email_address";
    private static final String PREFS_PASSWORD = "prefs_password";
    private static final String PREFS_USER = "prefs_user";
    private static final String PREFS_COUNTS = "prefs_counts";
    private static final String PREFS_ZAKAT_GOAL = "prefs_zakat_goal";
    private static final String PREFS_TOP_ZAKATIFIERS = "prefs_top_zakatifiers";
    private static final String PREFS_USER_MENU = "prefs_user_menu";
    private static final String PREFS_GOLD_VALUE = "prefs_gold_value";
    private static final String PREFS_CHAT_SESSIONS = "prefs_chat_sessions";
    private static final String PREFS_CATEGORIES = "prefs_categories";
    private static final String PREFS_INVITES = "prefs_invites";
    private static final String PREFS_USER_CATEGORIES = "prefs_user_categories";
    private static final String PREFS_USER_CHARITIES = "prefs_user_charities";
    private static final String PREFS_PHOTO_BASE64 = "prefs_photo_base64";
    private static final String PREFS_USER_ID = "prefs_user_id";
    private static final String PREFS_FULLNAME = "prefs_full_name";
    private static final String PREFS_FB_ACCESS_TOKEN = "prefs_fb_access_token";
    private static final String PREFS_FB_ID = "prefs_fb_id";
    private static final String PREFS_USER_SIGNED_IN = "prefs_signed_in";
    private static final String PREFS_HAS_LOGGED_IN = "prefs_has_logged_in";
    private static final String PREFS_HAS_LOGGED_IN_ONCE = "prefs_has_logged_in_once";
    private static final String PREFS_HAS_UPDATED_PROFILE = "prefs_has_updated_profile";
    private static final String PREFS_IS_FIRST_USE_ON_DEVICE = "prefs_is_first_use_on_device";
    private static final String PREFS_IS_ONBOARDING_ACTIVITY_SHOWN = "prefs_is_onboarding_activity_shown";
    private static final String PREFS_IS_PROFILE_ONBOARDED = "prefs_is_profile_onboarded";
    private static final String PREFS_IS_ZAKATIFY_ONBOARDED = "prefs_is_zakatify_onboarded";
    private static final String PREFS_IS_PAYMENT_ONBOARDED = "prefs_is_payment_onboarded";
    private static final String PREFS_IS_INTERESTS_ONBOARDED = "prefs_is_interests_onboarded";

    public static void resetUser(Context context) {
        SharedPrefsUtils.clearPreference(context, PREFS_USER);
        SharedPrefsUtils.clearPreference(context, PREFS_USER_SIGNED_IN);
        SharedPrefsUtils.clearPreference(context, PREFS_USER_ID);
        SharedPrefsUtils.clearPreference(context, PREFS_AUTH_TOKEN);
        SharedPrefsUtils.clearPreference(context, PREFS_CHAT_SESSIONS);
        SharedPrefsUtils.clearPreference(context, PREFS_INVITES);
        SharedPrefsUtils.clearPreference(context, PREFS_TOP_ZAKATIFIERS);

        //clearSocialData(context);
    }

    public static void clearOnBoardingPreferences(Context context) {
        SharedPrefsUtils.clearPreference(context, PREFS_IS_FIRST_USE_ON_DEVICE);
        SharedPrefsUtils.clearPreference(context, PREFS_IS_ONBOARDING_ACTIVITY_SHOWN);
        SharedPrefsUtils.clearPreference(context, PREFS_IS_PROFILE_ONBOARDED);
        SharedPrefsUtils.clearPreference(context, PREFS_IS_ZAKATIFY_ONBOARDED);
        SharedPrefsUtils.clearPreference(context, PREFS_IS_PAYMENT_ONBOARDED);
        SharedPrefsUtils.clearPreference(context, PREFS_IS_INTERESTS_ONBOARDED);
    }

    public static void clearSocialData(Context context) {
        SharedPrefsUtils.clearPreference(context, PREFS_FULLNAME);
        SharedPrefsUtils.clearPreference(context, PREFS_EMAIL_ADDRESS);
        SharedPrefsUtils.clearPreference(context, PREFS_PROFILE_PHOTO);
    }

    public static void saveDeviceId(Context context, String deviceId) {
        SharedPrefsUtils.setStringPreference(context, PREFS_DEVICE_ID, deviceId);
    }

    public static void saveAuthToken(Context context, String apiToken) {
        SharedPrefsUtils.setStringPreference(context, PREFS_AUTH_TOKEN, apiToken);
    }

    public static void saveTwitterEmailAddress(Context context, String emailAddress) {
        SharedPrefsUtils.setStringPreference(context, PREFS_TWITTER_EMAIL_ADDRESS, emailAddress);
    }

    public static void saveFCMToken(Context context, String FCMToken) {
        SharedPrefsUtils.setStringPreference(context, PREFS_FCM_TOKEN, FCMToken);
    }

    public static void saveUserId(Context context, String userId) {
        SharedPrefsUtils.setStringPreference(context, PREFS_USER_ID, userId);
    }

    public static void saveProfileData(Context context, Map<String, String> profileData) {
        SharedPrefsUtils.setStringPreference(context, PREFS_FULLNAME, profileData.get(Constants.FULL_NAME));
        SharedPrefsUtils.setStringPreference(context, PREFS_EMAIL_ADDRESS, profileData.get(Constants.EMAIL_ADDRESS));
        SharedPrefsUtils.setStringPreference(context, PREFS_PROFILE_PHOTO, profileData.get(Constants.PROFILE_PHOTO));
    }

    public static void saveFacebookAuthParams(Context context, Map<String, String> facebookAuthParams) {
        SharedPrefsUtils.setStringPreference(context, PREFS_FB_ACCESS_TOKEN, facebookAuthParams.get(Constants.FACEBOOK_ACCESS_TOKEN));
        SharedPrefsUtils.setStringPreference(context, PREFS_FB_ID, facebookAuthParams.get(Constants.FACEBOOK_ID));
    }

    public static void saveUser(Context context, User user) {
        SharedPrefsUtils.setStringPreference(context, PREFS_USER, StringUtils.userToString(user));
    }

    /**************************************************
     *
     *
     **************************************************/

    public static User getUser(Context context) {
        User user;
        user = StringUtils.userFromString(SharedPrefsUtils.getStringPreference(context, PREFS_USER));
        return user;
    }

    public static String getAuthToken(Context context) {
        return SharedPrefsUtils.getStringPreference(context, PREFS_AUTH_TOKEN);
    }

    public static String getFCMToken(Context context) {
        return SharedPrefsUtils.getStringPreference(context, PREFS_FCM_TOKEN);
    }

    public static String getDeviceId(Context context) {
        return SharedPrefsUtils.getStringPreference(context, PREFS_DEVICE_ID);
    }

    public static Map<String, String> getLoginData(Context context) {
        Map<String, String> loginDataMap = new HashMap<String, String>();
        loginDataMap.put(Constants.ID, SharedPrefsUtils.getStringPreference(context, PREFS_ID));
        loginDataMap.put(Constants.PASSWORD, SharedPrefsUtils.getStringPreference(context, PREFS_PASSWORD));
        return loginDataMap;
    }

    public static Map<String, String> getProfileData(Context context) {
        Map<String, String> profileData = new HashMap<String, String>();
        profileData.put(Constants.FULL_NAME, SharedPrefsUtils.getStringPreference(context, PREFS_FULLNAME));
        profileData.put(Constants.PROFILE_PHOTO, SharedPrefsUtils.getStringPreference(context, PREFS_PROFILE_PHOTO));
        return profileData;
    }

    public static Map<String, String> getFacebookAuthParams(Context context) {
        Map<String, String> loginDataMap = new HashMap<String, String>();
        loginDataMap.put(Constants.FACEBOOK_ACCESS_TOKEN, SharedPrefsUtils.getStringPreference(context, PREFS_FB_ACCESS_TOKEN));
        loginDataMap.put(Constants.FACEBOOK_ID, SharedPrefsUtils.getStringPreference(context, PREFS_FB_ID));
        return loginDataMap;
    }

    public static boolean getSignInStatus(Context context) {
        return SharedPrefsUtils.getBooleanPreference(context, PREFS_USER_SIGNED_IN, false);
    }

    public static boolean getLoggedInStatus(Context context) {
        return SharedPrefsUtils.getBooleanPreference(context, PREFS_HAS_LOGGED_IN, false);
    }

    public static boolean hasLoggedInOnce(Context context) {
        return SharedPrefsUtils.getBooleanPreference(context, PREFS_HAS_LOGGED_IN_ONCE, false);
    }

    public static boolean getProfileUpdateStatus(Context context) {
        return SharedPrefsUtils.getBooleanPreference(context, PREFS_HAS_UPDATED_PROFILE, false);
    }


    public static boolean isFirstUseOnDevice(Context context) {
        return SharedPrefsUtils.getBooleanPreference(context, PREFS_IS_FIRST_USE_ON_DEVICE, true);
    }
}
